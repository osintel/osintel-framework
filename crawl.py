#!/usr/bin/env python3
#   See LICENSE.TXT for full license info
#   created by A.J. Atkinson

import asyncio
import logging
import re
import signal
from colorama import Fore
import sys
import urllib.parse
from bs4 import BeautifulSoup
import aiohttp

url_regex = re.compile(r'(?i)href=["\']?([^\s"\'<>]+)')
email_regex = re.compile('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')


class Crawler:
    init_val = 1

    def __init__(self, maxtasks=10):

        self.session = aiohttp.ClientSession()
        self.rooturl = input("Enter target URL: ")
        self.todo = set()
        self.busy = set()
        self.emails_db = set()
        self.req_count = 0
        self.done = {}
        self.tasks = set()
        self.sem = asyncio.Semaphore(maxtasks)

    async def run(self):
        t = asyncio.ensure_future(self.addurls([(self.rooturl, '')]))
        await asyncio.sleep(1)
        while self.busy:
            await asyncio.sleep(1)
        await t
        await self.session.close()

    async def addurls(self, urls):
        for url, parenturl in urls:
            url = urllib.parse.urljoin(parenturl, url)
            url, frag = urllib.parse.urldefrag(url)
            if (url.startswith(self.rooturl) and
                    url not in self.busy and
                    url not in self.done and
                    url not in self.todo):
                print(Fore.YELLOW + url + Fore.WHITE)
                self.todo.add(url)
                await self.sem.acquire()
                task = asyncio.ensure_future(self.process(url))
                task.add_done_callback(lambda t: self.sem.release())
                task.add_done_callback(self.tasks.remove)
                self.tasks.add(task)
                self.init_val == 0

    async def addemails(self,email):
        self.emails_db.add(email)

    async def process(self, url):
        print(url)

        self.todo.remove(url)
        self.busy.add(url)
        try:
            self.req_count += 1
            resp = await self.session.get(url)
        except Exception as exc:
            print('...', url, 'has error', repr(str(exc)))
            self.done[url] = False
        else:
            if (resp.status == 200 and
                    ('text/html' in resp.headers.get('content-type'))):
                data = (await resp.read()).decode('utf-8', 'replace')
                urls = re.findall(url_regex, data)
                
                emails = re.findall(email_regex, data)
                for i in emails:
                    print(Fore.CYAN + i + Fore.WHITE)
                    await self.addemails(i)

                if urls:
                    asyncio.Task(self.addurls([(u, url) for u in urls]))
            resp.close()
            self.done[url] = True
        self.busy.remove(url)
        #print(len(self.done), 'completed tasks,', len(self.tasks),
        #      'still pending, todo', len(self.todo))
        #if self.init_val == 0 and len(self.tasks) == 1:
        #    self.loop.stop()

def main():
    print("Full URL should include schema (HTTP/HTTPS), FQDN (www.host.com), and starting page (optional)")
    c = Crawler()
    c.loop = asyncio.get_event_loop()
    asyncio.ensure_future(c.run())

    try:
        c.loop.add_signal_handler(signal.SIGINT, c.loop.stop)
        c.loop.run_until_complete(c.run())
    except RuntimeError:
        pass
    except KeyboardInterrupt:
        print("DONE!")

    print(Fore.YELLOW)
    for i in c.emails_db:
        print(i)
    #print('todo:', len(c.todo))
    #print('busy:', len(c.busy))
    #print('done:', len(c.done), '; ok:', sum(c.done.values()))
    #print('tasks:', len(c.tasks))
        print(Fore.WHITE)


if __name__== '__main__':
    main()
